

class searchModel{
  final String image;
  final String title;
  final String description;

  searchModel({
    this.image = '',
    this.title = '',
    this.description = ''
  });

  factory searchModel.fromJson(Map<String,dynamic> json) =>
      searchModel(
        image: json['image'],
        title: json['title'],
        description: json['description'],
      );
}