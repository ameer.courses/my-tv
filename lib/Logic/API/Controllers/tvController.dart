// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:http/http.dart' as http;
import 'package:my_tv/Logic/API/Models/searchModel.dart';
import 'dart:async';
import 'dart:convert';

import 'package:my_tv/Logic/API/Models/tvModel.dart';

class TvController{

  static Future<List<tvModel>> getTop250TVs() async {
      var response = await http.get(Uri.parse('https://jsonkeeper.com/b/C8LP'));
      Map<String,dynamic> json = jsonDecode(response.body);

      List<dynamic> items = json['items'];
      List<tvModel> models = [];

      for(var item in items)
        models.add( tvModel.fromJson(item) );

      return models;
  }

  static Future<List<searchModel>> search(String value) async {
    var response = await http.get(Uri.parse('https://imdb-api.com/en/API/Search/k_c5mb66ix/$value'));
    Map<String,dynamic> json = jsonDecode(response.body);

    List<dynamic> results = json['results'];
    List<searchModel> models = [];

    for(var m in results)
      models.add(searchModel.fromJson(m));

    return models;

  }
  
}