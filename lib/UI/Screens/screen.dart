// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class Screen extends StatelessWidget {
  final Widget? body;
  final String? title;
  final List<Widget>? actions;
  final bool enableDrawer;

  Screen({this.body, this.title,this.actions,this.enableDrawer = true});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.tv),
            Text(title ?? 'no title'),
          ],
        ),
        elevation: 0.0,
        actions: actions,
      ),
      body: body,
      drawer:enableDrawer ?
          Drawer(
            child: ListView(
              children: [
                DrawerHeader(
                    child: Container(
                      color: Colors.blueAccent,
                    ),
                  padding: EdgeInsets.zero,
                ),
                Card(
                  child: ListTile(
                    title: Text('Top movies'),
                    //subtitle: Text('Top 250 Movies'),
                    leading: Icon(Icons.movie_filter),
                    trailing: Icon(Icons.arrow_forward),
                  ),
                ),
                Card(
                  child: ListTile(
                    title: Text('Top TVs'),
                    //subtitle: Text('Top 250 Movies'),
                    leading: Icon(Icons.tv),
                    trailing: Icon(Icons.arrow_forward),
                  ),
                ),
                Card(
                  child: ListTile(
                    title: Text('Settings'),
                    //subtitle: Text('Top 250 Movies'),
                    leading: Icon(Icons.settings),
                    trailing: Icon(Icons.arrow_forward),
                  ),
                ),
              ],
            ),
          )
          : null,
    );
  }
}
