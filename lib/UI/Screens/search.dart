// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:my_tv/UI/Screens/APIView/SearchResultScreen.dart';

class SearchScreen extends StatefulWidget {
  final TextEditingController controller = TextEditingController();
  static final List<String> history = [];
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: theme.scaffoldBackgroundColor,
        foregroundColor: Colors.black,
        title: TextField(
          decoration: InputDecoration(
            hintText: 'Search',
            prefixIcon: IconButton(
              icon: Icon(Icons.close),
              onPressed: (){
                widget.controller.text = '';
              },
            ),
            suffixIcon: IconButton(
              icon: Icon(Icons.search),
              onPressed: (){
                if(!SearchScreen.history.contains(widget.controller.text))
                setState(() {
                  SearchScreen.history.add(widget.controller.text);
                });
                if(widget.controller.text.isNotEmpty)
                  Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => SearchResultScreen(widget.controller.text))
                  );
              },
            ),
          ),
          controller: widget.controller,
        ),
      ),
      body: ListView.separated(
          itemCount: SearchScreen.history.length,
          itemBuilder: (context,i){
            return ListTile(
              title: Text(SearchScreen.history[i]),
              onTap: (){
                widget.controller.text = SearchScreen.history[i];
              },
              trailing: IconButton(
                icon: Icon(Icons.close),
                onPressed: (){
                  setState(() {
                    SearchScreen.history.removeAt(i);
                  });
                },
              ),
            );
          },
        separatorBuilder: (context,i){
            return Divider();
        },
      )
    );
  }
}
