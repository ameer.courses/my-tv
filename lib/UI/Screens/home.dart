// ignore_for_file: curly_braces_in_flow_control_structures, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:my_tv/Logic/API/Controllers/tvController.dart';
import 'package:my_tv/Logic/API/Models/tvModel.dart';
import 'package:my_tv/UI/Screens/screen.dart';
import 'package:my_tv/UI/Widgets/APIView/tvView.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Screen(
      title: 'Home',
      body: FutureBuilder<List<tvModel>>(
        future: TvController.getTop250TVs(),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return SingleChildScrollView(
                child: Wrap(
                  children: [
                    for( var m in snapShot.data!)
                      TvView(m),
                  ],
                ),
                scrollDirection: Axis.vertical,
            );

          if(snapShot.hasError)
            return Center(
              child: Text(snapShot.error.toString()),
            );
          // loading
          return Center(
            child: CircularProgressIndicator(),
          );

        },
      ),
      actions: [
        IconButton(
            onPressed: (){
              Navigator.pushNamed(context, '/search');
            },
            icon: Icon(Icons.search)
        )
      ],
    );
  }
}
