// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:my_tv/Logic/API/Controllers/tvController.dart';
import 'package:my_tv/Logic/API/Models/searchModel.dart';

import '../screen.dart';

class SearchResultScreen extends StatelessWidget {
  final String value;


  SearchResultScreen(this.value);

  @override
  Widget build(BuildContext context) {
    return Screen(
      enableDrawer: false,
      title: value,
      body: FutureBuilder<List<searchModel>>(
        future: TvController.search(value),
        builder: (context,snapShot){
          if(snapShot.hasData)
            return ListView.builder(
              itemCount: snapShot.data!.length,
              itemBuilder: (context,i){
                return Card(
                  child: ListTile(
                    title: Text(snapShot.data![i].title),
                    subtitle: Text(snapShot.data![i].description),
                    leading: CircleAvatar(
                      foregroundImage: NetworkImage(snapShot.data![i].image),
                    ),
                  ),
                );
              },
            );

          if(snapShot.hasError)
            return Center(
              child: Text(snapShot.error.toString()),
            );
          // loading
          return Center(
            child: CircularProgressIndicator(),
          );

        },
      ),
    );
  }
}
