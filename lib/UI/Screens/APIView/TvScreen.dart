import 'dart:math';

import 'package:flutter/material.dart';
import 'package:my_tv/Logic/API/Models/tvModel.dart';
import 'package:my_tv/UI/Screens/screen.dart';

class TvScreen extends StatelessWidget {

  final tvModel model;


  TvScreen(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Screen(
      title: model.title,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: size.height*0.6,
              child: Image.network(
                  model.image,
                  fit: BoxFit.cover,
              ),
            ),
            InfoRow(title: 'Full title', content: model.fullTitle),
            InfoRow(title: 'Global rank', content: model.rank),
            InfoRow(title: 'year', content: model.year),
            InfoRow(title: 'Crew', content: model.crew),
            InfoRow(title: 'Rating', content: '${model.imDbRating}/10'),
            InfoRow(title: 'Rating Count', content: model.imDbRatingCount),
          ],
        ),
      ),
      enableDrawer: false,
    );
  }
}

class InfoRow extends StatelessWidget {

  final String title;
  final String content;


  InfoRow({required this.title,required this.content});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(size.width*0.025),
      padding: EdgeInsets.all(size.width*0.025),
      child: FittedBox(
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            Text('$title : '),
            Text(content)
          ],
        ),
      ),
      height: size.height*0.1,
      width: double.infinity,
    );
  }
}
