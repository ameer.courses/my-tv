import 'package:flutter/material.dart';
import 'package:my_tv/Logic/API/Models/tvModel.dart';
import 'dart:math';

import 'package:my_tv/UI/Screens/APIView/TvScreen.dart';


class TvView extends StatelessWidget {

  final tvModel model;


  TvView(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double len = min(size.width,size.height);
    return GestureDetector(
      child: Container(
        width: len * 0.45,
        height: len * 0.75,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(len*0.025),
          color: Colors.grey.withOpacity(0.5)
        ),
        margin: EdgeInsets.all(len*0.025),
        child: Column(
          children: [
            Expanded(
              flex: 3,
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(len*0.025)),
                    image: DecorationImage(
                      image: NetworkImage(model.image),
                      fit: BoxFit.cover
                    )
                  ),
                )
            ),
            Expanded(
                child: ListTile(
                  title: Text(model.title),
                  subtitle: Row(
                    children: [
                      Icon(Icons.star,color: Colors.amber,),
                      Text('${model.imDbRating}/10'),
                    ],
                  ),
                  trailing: Text(model.rank),
                )
            ),
          ],
        ),
      ),
      onTap: (){
        Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => TvScreen(model)
        ),
        );
      },
    );
  }
}
